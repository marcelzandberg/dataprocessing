# Dataprocessing

A snakemake pipline for analysis from genome sequencing

## Includes

This repo contains 5 cases for training the final produced pipeline is in the folder casus_finalassignment.
This folder contains:

* Config file with parameters and paths to files 
* The actual data to ran the Snakefile on
* A picture describing the elements with in the pipeline
* The Snakefile it self


# Putting the pipeline together

- rules in a snakefile: steps in the workflow
- dependencies between steps automatically determined by matching file names
- Input and output specify lists of file names
- {input} contains multiple values, which will be concatenated and separated by whitespace missing output directories are automatically created
- When executing the workflow Snakemake tries to generate the given target files, which is called a job.  e.g. `snakemake mapped_reads/A.bam`

- A job will not be reran if the output already exists, except if one of the input files is newer than one of the output files (or will be updated by another job).


## The actual steps (rules)

![poster](./casus_finalassignment/dag2.png "Poster")


## prerequisites

Things you need to install to be able to run this pipeline

* Burrows-Wheeler Aligner | [BWA download page] (https://sourceforge.net/projects/bio-bwa/files/)

* Samtools | [Samtools download page] (http://www.htslib.org/download/)

* bcftools | [bcftools download page] (http://www.htslib.org/download/)


## Installing snakemake
 
Snakemake is a python3 package, so you need a python3 environment. Create a virtual environment by typing in your terminal the following:

```
virtualenv -p /usr/bin/python3 venv
source venv/bin/activate

#install snakemake
pip3 install snakemake
```


## Deployment

* step 1 - start the virtualenv
    * open a terminal and type ```source venv/bin/activate``` from the root where you have your venv folder installed
* step 2 - go to the folder where the snakefile is stored
    * example ```cd folder/to/snakefile```
* step 3 - now run the actual Snakefile
    * ```snakemake --snakefile Snakefile```


## Results

The rule shall generate a histogram of the quality scores that have been assigned to the variant


## Author

* **Marcel Zandberg** - *Initial work* - [Portfolio](https://bioinf.nl/~mazandberg) | [Bitbucket](https://bitbucket.org/marcelzandberg)
